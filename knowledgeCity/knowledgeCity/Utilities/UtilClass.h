//
//  UtilClass.h
//  knowledgeCity
//
//  Created by Sufyan Anees on 04/10/2019.
//  Copyright © 2019 Sufyanz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UtilClass : NSObject

+ (UIColor *)colorFromHexString:(NSString *)hex alpha:(CGFloat)alpha;

@end

NS_ASSUME_NONNULL_END
