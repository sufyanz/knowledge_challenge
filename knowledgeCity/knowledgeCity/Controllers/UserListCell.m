//
//  UserListCell.m
//  KnowledgeCity
//
//  Created by Sufyan Anees on 07/10/2019.
//  Copyright © 2019 Sufyanz. All rights reserved.
//

#import "UserListCell.h"


@implementation UserListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
