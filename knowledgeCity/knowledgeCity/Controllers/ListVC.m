//
//  ListVC.m
//  KnowledgeCity
//
//  Created by Sufyan Anees on 05/10/2019.
//  Copyright © 2019 Sufyanz. All rights reserved.
//

#import "ListVC.h"
#import "UserListCell.h"
#import "UtilClass.h"
#import "SVProgressHUD.h"
#import "ViewController.h"
#import "AppDelegate.h"
#import "AFNetworking.h"

#define appUrl @"https://api.kcdev.pro/v2/accounts/"
#define appAuthUrl @"http://api.kcdev.pro/v2/auth"

#define textColor @"333333"
#define orangeColor @"E98C3B"

@interface ListVC ()<UITableViewDelegate, UITableViewDataSource>
{
    NSArray *listData;
    int pageNumber;
}

@property (weak, nonatomic) IBOutlet UITableView *userTable;

@end

@implementation ListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    pageNumber = 0;
    UIButton *btn = [self.view viewWithTag:101];
    [btn setTitleColor:[UtilClass colorFromHexString:orangeColor alpha:1.0] forState:UIControlStateNormal];
    UIButton *btnPrev = [self.view viewWithTag:100];
    btnPrev.hidden = YES;
    [self loadListData];
}


-(void)loadListData{
    //https://api.kcdev.pro/v2/accounts/[account_id]/students?_start=0&_limit=5&token=[token]
    
    NSString *qString = [NSString stringWithFormat:@"%@/students?_start=%d&_limit=5&token=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"accountid"],pageNumber,[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
    
    [self callGETRequestForQueryString:qString contentType:@"application/x-www-form-urlencoded" HTTPBodyObject:nil];
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
    
}
-(CGFloat )tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"UserListCell";
    
    UserListCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier
                                                               forIndexPath:indexPath];

    cell.groupLabel.text = listData[indexPath.row][@"group"];
    cell.usernameLabel.text = listData[indexPath.row][@"username"];
    cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@",listData[indexPath.row][@"first_name"],listData[indexPath.row][@"last_name"]];
    
    if(indexPath.row % 2 == 0){
        cell.contentView.backgroundColor = [UtilClass colorFromHexString:@"ebebeb" alpha:1.0];
    }else{
        cell.contentView.backgroundColor = [UIColor clearColor];
    }
    
    
    
    return cell;
}




- (void)callGETRequestForQueryString:(NSString*)queryString contentType:(NSString*)contentType HTTPBodyObject:(id)httpBodyObject{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
   AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
    __weak typeof(self) weakSelf = self;
               NSError *error = nil;
               NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:[NSString stringWithFormat:@"%@%@",appUrl,queryString] parameters:nil error:&error];
               [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [SVProgressHUD show];
               NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                       [SVProgressHUD dismiss];
            
                   if (error) {
                       UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Something went wrong" preferredStyle:UIAlertControllerStyleAlert];
                               [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",@"") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                                   
                               }]];
                               
                               [weakSelf presentViewController:alert animated:YES completion:nil];
                   }
                   else{
                       
                       self->listData = [NSArray arrayWithArray:responseObject[@"response"]];
                       [self.userTable reloadData];
                       
                   
                   }
               }];
               [dataTask resume];
    
}



-(IBAction)onPagingButton:(UIButton *)btn{
    
    if(btn.tag > 100 && btn.tag < 200){
        pageNumber = (int)(btn.tag - 100);
        
        for (int i=101; i<105; i++) {
            UIButton *btn1 = [self.view viewWithTag:i];
            [btn1 setTitleColor:[UtilClass colorFromHexString:textColor alpha:1.0] forState:UIControlStateNormal];
        }
        [btn setTitleColor:[UtilClass colorFromHexString:orangeColor alpha:1.0] forState:UIControlStateNormal];
        
        [self loadListData];
        
    }else{
        
        if(btn.tag == 100){
            pageNumber = pageNumber - 1;
            for (int i=101; i<105; i++) {
                    UIButton *btn1 = [self.view viewWithTag:i];
                    [btn1 setTitleColor:[UtilClass colorFromHexString:textColor alpha:1.0] forState:UIControlStateNormal];
                }
            UIButton *btn2 = [self.view viewWithTag:(pageNumber + 100)];
            [btn2 setTitleColor:[UtilClass colorFromHexString:orangeColor alpha:1.0] forState:UIControlStateNormal];
            
            [self loadListData];
            
            
            
        } else{
            
            pageNumber = pageNumber + 1;
                    for (int i=101; i<105; i++) {
                            UIButton *btn1 = [self.view viewWithTag:i];
                            [btn1 setTitleColor:[UtilClass colorFromHexString:textColor alpha:1.0] forState:UIControlStateNormal];
                        }
                    UIButton *btn2 = [self.view viewWithTag:(pageNumber + 100)];
                    [btn2 setTitleColor:[UtilClass colorFromHexString:orangeColor alpha:1.0] forState:UIControlStateNormal];
                    
                    [self loadListData];
            
            
        }
        
    }
    
    if(pageNumber == 1){
              UIButton *btnPrev = [self.view viewWithTag:100];
                btnPrev.hidden = YES;
          
          }else{
              
              UIButton *btnPrev = [self.view viewWithTag:100];
                          btnPrev.hidden = NO;
          }
          
          if(pageNumber == 4){
                  UIButton *btnNext = [self.view viewWithTag:200];
                    btnNext.hidden = YES;
              
              }else{
                  
                  UIButton *btnNext = [self.view viewWithTag:200];
                              btnNext.hidden = NO;
              }
    
    
    
}

-(IBAction)onLogout:(id)sender{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"],@"token", nil];
     
     [self callDeleteRequestForQueryString:@"" contentType:@"application/x-www-form-urlencoded" HTTPBodyObject:parameters];
    
    
}


- (void)callDeleteRequestForQueryString:(NSString*)queryString contentType:(NSString*)contentType HTTPBodyObject:(id)httpBodyObject{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
   AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
    __weak typeof(self) weakSelf = self;
               NSError *error = nil;
               NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"DELETE" URLString:[NSString stringWithFormat:@"%@%@",appAuthUrl,queryString] parameters:httpBodyObject error:&error];
               [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [SVProgressHUD show];
               NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                       [SVProgressHUD dismiss];
            
                   if (error) {
                       UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Something went wrong" preferredStyle:UIAlertControllerStyleAlert];
                               [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",@"") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                                   
                               }]];
                               
                               [weakSelf presentViewController:alert animated:YES completion:nil];
                   }
                   else{
                       
                       
                       ViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                                
                                  
                                        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                        [appDelegate.window makeKeyAndVisible];
                                       // [self.window.rootViewController presentViewController:viewController
                                                                                  //   animated:animated
                                                                                  // completion:nil];
                                        
                                        [appDelegate.window setRootViewController:controller];
                       
                   
                   }
               }];
               [dataTask resume];
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
