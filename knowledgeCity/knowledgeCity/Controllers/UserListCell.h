//
//  UserListCell.h
//  KnowledgeCity
//
//  Created by Sufyan Anees on 07/10/2019.
//  Copyright © 2019 Sufyanz. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *groupLabel;


@end

NS_ASSUME_NONNULL_END
