//
//  ViewController.m
//  knowledgeCity
//
//  Created by Sufyan Anees on 04/10/2019.
//  Copyright © 2019 Sufyanz. All rights reserved.
//

#import "ViewController.h"
#import "UtilClass.h"
#import "SVProgressHUD.h"
#import "AFNetworking.h"
#import "ListVC.h"
#import "AppDelegate.h"

#define eborderColor @"808080"
#define orangeColor @"E98C3B"
#define appUrl @"http://api.kcdev.pro/v2/auth"


@interface ViewController ()<
UITextFieldDelegate>{
    BOOL isRememeber;
}

@property (weak, nonatomic) IBOutlet UIView *loginView;
@property (weak, nonatomic) IBOutlet UIView *passView;
@property (weak, nonatomic) IBOutlet UIView *radioMainView;
@property (weak, nonatomic) IBOutlet UIView *radioColorView;
@property (weak, nonatomic) IBOutlet UIView *buttonView;

@property (weak, nonatomic) IBOutlet UITextField *loginText;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;

@property (weak, nonatomic) IBOutlet UIImageView *loginImage;
@property (weak, nonatomic) IBOutlet UIImageView *passwordImage;

@property (nonatomic, strong) AFHTTPRequestSerializer *requestSerializer;




@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    isRememeber = NO;
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"login"]){
        isRememeber = YES;
        self.loginText.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"login"];
        self.passwordText.text =  [[NSUserDefaults standardUserDefaults] objectForKey:@"pass"];
          
        
    }
    
    _loginView.layer.cornerRadius = _loginView.frame.size.height / 2;
    _loginView.layer.borderColor = [UtilClass colorFromHexString:eborderColor alpha:1.0].CGColor;
    _loginView.layer.borderWidth = 1.0;
    
    _loginImage.tintColor = [UtilClass colorFromHexString:eborderColor alpha:1.0];
    _passwordImage.tintColor = [UtilClass colorFromHexString:eborderColor alpha:1.0];
    
    _passView.layer.cornerRadius = _passView.frame.size.height / 2;
    _passView.layer.borderColor = [UtilClass colorFromHexString:eborderColor alpha:1.0].CGColor;
    _passView.layer.borderWidth = 1.0;
    
    _buttonView.layer.cornerRadius = _buttonView.frame.size.height / 2;

    
    _radioColorView.layer.cornerRadius = _radioColorView.frame.size.height / 2;
    
       _radioMainView.layer.cornerRadius = _radioMainView.frame.size.height / 2;
    _radioMainView.layer.borderColor = [UtilClass colorFromHexString:eborderColor alpha:1.0].CGColor;
      _radioMainView.layer.borderWidth = 2.0;
    
    //self.loginText.text = @"devtest895";
    //self.passwordText.text = @"4mcR6jtUw";
    
    _buttonView.layer.masksToBounds = NO;
    [_buttonView.layer setShadowColor:[UIColor blackColor].CGColor];
    [_buttonView.layer setShadowOpacity:0.20];
    [_buttonView.layer setShadowRadius:10.0];
    [_buttonView.layer setShadowOffset:CGSizeMake(0.0, 1.0)];
    
    if(isRememeber == YES){
         _radioColorView.hidden = NO;
     }else{
         _radioColorView.hidden = YES;
     }
    
}


///TextField Delegates

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    if(textField.tag == 101){
          _loginView.layer.borderColor = [UtilClass colorFromHexString:orangeColor alpha:1.0].CGColor;
             _loginImage.tintColor = [UtilClass colorFromHexString:orangeColor alpha:1.0];
    }
    
    if(textField.tag == 102){
          _passView.layer.borderColor = [UtilClass colorFromHexString:orangeColor alpha:1.0].CGColor;
                 _passwordImage.tintColor = [UtilClass colorFromHexString:orangeColor alpha:1.0];
    }
    
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];

    [self.view endEditing:YES];
    
    if(textField.tag == 101){
            _loginView.layer.borderColor = [UtilClass colorFromHexString:eborderColor alpha:1.0].CGColor;
        _loginImage.tintColor = [UtilClass colorFromHexString:eborderColor alpha:1.0];

      }
      
      if(textField.tag == 102){
            _passView.layer.borderColor = [UtilClass colorFromHexString:eborderColor alpha:1.0].CGColor;
                  _passwordImage.tintColor = [UtilClass colorFromHexString:eborderColor alpha:1.0];
      }
    return YES;
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.

}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
}

///User Methods

-(IBAction)onRememberMe:(id)sender{
    
    if(isRememeber == YES){
        _radioColorView.hidden = YES;
        isRememeber = NO;
    }else{
        _radioColorView.hidden = NO;
        isRememeber = YES;
    }
    
    
}

-(IBAction)onLogIn:(id)sender{
 
    if(self.loginText.text.length == 0 || self.passwordText.text.length == 0){
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Please enter login and password" preferredStyle:UIAlertControllerStyleAlert];
         [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",@"") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
             
         }]];
         
         [self presentViewController:alert animated:YES completion:nil];
        
    }else{

        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.loginText.text,@"username",self.passwordText.text,@"password",@"accountAdmin",@"usertype",@"user",@"_extend", nil];
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:appUrl]
        sessionConfiguration:configuration];
        
        manager.requestSerializer = self.requestSerializer;
        
        
        [self callPOSTRequestForQueryString:@"" contentType:@"application/x-www-form-urlencoded" HTTPBodyObject:parameters];
        
        
    }
    
}

- (AFHTTPRequestSerializer*)requestSerializer
{
    if(!_requestSerializer){
        _requestSerializer = [[AFJSONRequestSerializer alloc]init];
        [_requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [_requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    }
    return _requestSerializer;
}


- (void)callPOSTRequestForQueryString:(NSString*)queryString contentType:(NSString*)contentType HTTPBodyObject:(id)httpBodyObject{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
   AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
    __weak typeof(self) weakSelf = self;
               NSError *error = nil;
               NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@",appUrl,queryString] parameters:httpBodyObject error:&error];
               [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [SVProgressHUD show];
               NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                       [SVProgressHUD dismiss];
            
                   if (error) {
                       UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Wrong username or password" preferredStyle:UIAlertControllerStyleAlert];
                               [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK",@"") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                                   
                               }]];
                               
                               [weakSelf presentViewController:alert animated:YES completion:nil];
                   }
                   else{
                       
                       if(self->isRememeber == YES){
                           [NSUserDefaults.standardUserDefaults setObject:weakSelf.loginText.text forKey:@"login"];
                           [NSUserDefaults.standardUserDefaults setObject:weakSelf.passwordText.text forKey:@"pass"];
                           [NSUserDefaults.standardUserDefaults synchronize];
                       }else{
                           [NSUserDefaults.standardUserDefaults setObject:nil forKey:@"login"];
                           [NSUserDefaults.standardUserDefaults setObject:nil forKey:@"pass"];
                           [NSUserDefaults.standardUserDefaults synchronize];
                           
                       }
                       
                       NSDictionary *data =  (NSDictionary *)responseObject;
                       NSString *token = data[@"response"][@"token"];
                       NSString *accountId = data[@"response"][@"user"][@"account_id"];
                    
                       [NSUserDefaults.standardUserDefaults setObject:token forKey:@"token"];
                       [NSUserDefaults.standardUserDefaults setObject:accountId forKey:@"accountid"];
                       [NSUserDefaults.standardUserDefaults synchronize];
                       
                       
                       ListVC *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ListVC"];
                
                  
                        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                        [appDelegate.window makeKeyAndVisible];
                       // [self.window.rootViewController presentViewController:viewController
                                                                  //   animated:animated
                                                                  // completion:nil];
                        
                        [appDelegate.window setRootViewController:controller];
                       
                   
                   }
               }];
               [dataTask resume];
    
}


@end
