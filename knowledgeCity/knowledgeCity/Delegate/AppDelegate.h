//
//  AppDelegate.h
//  knowledgeCity
//
//  Created by Sufyan Anees on 04/10/2019.
//  Copyright © 2019 Sufyanz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@end

